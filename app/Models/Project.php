<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Project
 *
 * @property int $id
 * @property int $domain_id
 * @property string $app_key APP_KEY из env
 * @property string $git_branch имя ветки на гите
 * @property string $donor_url
 * @property string|null $open_date
 * @property int $google_analitic_enable
 * @property int $google_webmaster_enable
 * @property int $template_id
 * @property int $theme_id
 * @property int|null $google_id
 * @property int|null $twitter_account_id
 * @property int|null $facebook_account_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Domain $domain
 * @property-read \App\Models\GoogleAccount $googleAccount
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SocialPost[] $socialPosts
 * @property-read \App\Models\Template $template
 * @property-read \App\Models\Theme $theme
 * @mixin \Eloquent
 */
class Project extends Model
{
    protected $table = 'projects';
    
    public function domain()
    {
        return $this->hasOne('App\Models\Domain', 'id', 'domain_id');
    }
    
    public function template()
    {
        return $this->hasOne('App\Models\Template', 'id', 'template_id');
    }
    
    public function theme()
    {
        return $this->hasOne('App\Models\Theme', 'id', 'theme_id');
    }
    
    public function googleAccount()
    {
        return $this->hasOne('App\Models\GoogleAccount', 'id', 'google_id');
    }
    
    public function socialPosts()
    {
        return $this->hasMany('App\Models\SocialPost', 'project_id', 'id');
    }

    public function TwitterAccount()
    {
        return $this->hasOne('App\Models\TwitterAccount', 'id', 'twitter_account_id');
    }

    public function FacebookAccount()
    {
        return $this->hasOne('App\Models\FacebookAccount', 'id', 'facebook_account_id');
    }

    public function getRepostInfo($socialAccountId, $socialType)
    {
        $domain = Domain::find($this->domain_id);
        
        if(!$domain)
        {
            return false;
        }
        
        $json = file_get_contents('http://' .  $domain->name . '/api/social/getPostUrl?key=' .  urlencode($this->app_key)  . '&social_account_id=' . $socialAccountId . '&social_type=' . $socialType);
        
        if(!$json)
        {
            return false;
        }
        
        $repost = json_decode($json, true);
        
        if($repost['status'] == true)
        {
            return $repost['result'];
        }
        else
        {
            return false;
        }
            
    }
}
