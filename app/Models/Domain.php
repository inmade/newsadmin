<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Domain
 *
 * @property int $id
 * @property string $name
 * @property string|null $registrar Информация о регистраторе
 * @property string $register_date
 * @property string $next_pay Дата следующего платежа
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class Domain extends Model
{
    protected $table = 'domains';
}
