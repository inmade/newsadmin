<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmailAccount
 *
 * @property int $id
 * @property string $host
 * @property string $email
 * @property string $username
 * @property string $passwors
 * @property string $service
 * @property int|null $phone_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class EmailAccount extends Model
{
    protected $table = 'emails';
}
