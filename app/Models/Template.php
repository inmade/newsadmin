<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Template
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects
 * @mixin \Eloquent
 */
class Template extends Model
{
    protected $table = 'templates';
    
    public function projects()
    {
        return $this->hasMany('App\Models\Project', 'template_id', 'id');
    }
}
