<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SocialPost
 *
 * @property int $id
 * @property int $project_id
 * @property int $post_id
 * @property int $social_account_id
 * @property string $social_type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class SocialPost extends Model
{
    protected $table = 'social_post';
    
    const SOCIAL_TYPE_FACEBOOK  = 'facebook';
    const SOCIAL_TYPE_REDDIT    = 'reddit';
    const SOCIAL_TYPE_TWITTER   = 'twitter';

    protected $fillable = [
        'post_id',
        'social_account_id',
        'social_type'
    ];
}
