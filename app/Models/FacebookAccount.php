<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FacebookAccount
 *
 * @property int $id
 * @property int $email_id
 * @property string $username
 * @property string $password
 * @property int|null $phone_id
 * @property string $name
 * @property string $surname
 * @property string $gender
 * @property string $birthdate
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\EmailAccount $email
 * @mixin \Eloquent
 */
class FacebookAccount extends Model
{
    protected $table = 'facebook_accounts';
    
    public function email()
    {
        return $this->hasOne('App\Models\EmailAccount', 'id', 'email_id');
    }
}
