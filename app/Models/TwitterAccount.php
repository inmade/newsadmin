<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TwitterAccount
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property int $email_id
 * @property int $phone_id
 * @property string $consumer_key
 * @property string $consumer_secret
 * @property string $access_token
 * @property string $access_token_secret
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class TwitterAccount extends Model
{
    protected $table = 'twitter_accounts';
}
