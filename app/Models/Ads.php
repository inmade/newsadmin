<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ads
 *
 * @property int $id
 * @property string $name Уникальное имя блока
 * @property string $body Содержимое рекламного блока base64
 * @property string $description Описание рекламного блока
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class Ads extends Model
{
    protected $table = 'ad_blocks';
}
