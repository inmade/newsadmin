<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\GoogleAccount
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class GoogleAccount extends Model
{
    protected $table = 'google_accounts';
}
