<?php

namespace App\Extensions;

/**
 * Description of Proxy
 *
 * @author user
 */
class Proxy {
    
    static function refresh()
    {
        $res = shell_exec('sudo service tor restart 2>&1 &');
        if(  strpos( $res, 'Redirecting to /bin/systemctl restart tor.service' ) === 0 || $res == '')
            return true;
        else
            return false;

    }
}
