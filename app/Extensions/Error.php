<?php

namespace App\Extensions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Error {
    /* Константы описывающие типы ошибок */

    /**
     * parser: content empty
     */
    const PARSER_CONTENT_EMPTY = [
        'code'    => 1,
        'group'   => 'parser',
        'message' => 'content empty'
    ];

    /**
     * parser: cant parse html
     */
    const PARSER_CANT_PARSE_HTML = [
        'code'    => 2,
        'group'   => 'parser',
        'message' => 'cant parse html'
    ];

    /**
     * parser: element not found
     */
    const PARSER_ELEMENT_NOT_FOUND = [
        'code'    => 3,
        'group'   => 'parser',
        'message' => 'element not found'
    ];

    /**
     * parser: parameter must be array
     */
    const PARSER_PARAMETER_MUST_BE_ARRAY = [
        'code'    => 4,
        'group'   => 'parser',
        'message' => 'parameter must be array'
    ];

    /**
     * parser: parameter must be string
     */
    const PARSER_PARAMETER_MUST_BE_STRING = [
        'code'    => 5,
        'group'   => 'parser',
        'message' => 'parameter must be string'
    ];

    /**
     * parser: parameter "get" not found
     */
    const PARSER_PARAMETER_NOT_FOUND = [
        'code'    => 6,
        'group'   => 'parser',
        'message' => 'parameter not found'
    ];

    /**
     * model: cant update
     */
    const MODEL_CANT_UPDATE = [
        'code'    => 7,
        'group'   => 'model',
        'message' => 'cant update'
    ];

    /**
     * image: cant download image
     */
    const IMAGE_DOWNLOAD = [
        'code'    => 8,
        'group'   => 'image',
        'message' => 'cant download image'
    ];

    /**
     * image: cant save image
     */
    const IMAGE_SAVE = [
        'code'    => 9,
        'group'   => 'image',
        'message' => 'cant save image'
    ];

    /**
     * directory: cant create directory
     */
    const DIRECTORY_CREATE = [
        'code'    => 10,
        'group'   => 'directory',
        'message' => 'cant create directory'
    ];

    /**
     * http: cant solve http request
     */
    const HTTP_REQUEST = [
        'code'    => 11,
        'group'   => 'http',
        'message' => 'cant solve http request'
    ];

    /**
     * translate: cant parse json response
     */
    const TRANSLATE_JSON_PASRE = [
        'code'    => 12,
        'group'   => 'translate',
        'message' => 'cant parse json response'
    ];

    /**
     * translate: cant parse json response
     */
    const TRANSLATE_JSON_CHANGE = [
        'code'    => 13,
        'group'   => 'translate',
        'message' => 'elment with result text not found'
    ];

    /**
     * translate: cant parse json response
     */
    const TRANSLATE_ALREADY_EXIST = [
        'code'    => 14,
        'group'   => 'translate',
        'message' => 'htis post already translated'
    ];

    /**
     * translate: title or text in post not required
     */
    const TRANSLATE_NOT_REQUIRED = [
        'code'    => 15,
        'group'   => 'translate',
        'message' => 'title or text in post not required'
    ];

    /**
     * translate: title or text in post not required
     */
    const TWITTER_POST = [
        'code'    => 16,
        'group'   => 'twitter',
        'message' => 'cant post tweet'
    ];

    /**
     * content: Title in post is empty
     */
    const CONTENT_TITLE_EMPTY = [
        'code'    => 17,
        'group'   => 'content',
        'message' => 'title in post empty'
    ];

    /**
     * content: Text in post is empty
     */
    const CONTENT_TEXT_EMPTY = [
        'code'    => 18,
        'group'   => 'content',
        'message' => 'text in post empty'
    ];

    /**
     * content: Text in post is empty
     */
    const PINTEREST_POST = [
        'code'    => 19,
        'group'   => 'pinterest',
        'message' => 'cant post pinterest'
    ];

    public $code;
    public $group;
    public $message;
    public $parameters;

    /**
     * Создает объект описывающий ошибку типа $type.
     *
     * @param const $arrayType Тип ошибки, одна из констант данного класса.
     * @param array $parameters Параметры и атрибуты в формате ключ => значение
     *
     * @return void.
     */
    public function __construct ($type, $parameters = []) {
        $this->code    = $type['code'];
        $this->group   = $type['group'];
        $this->message = $type['message'];
        if( $parameters )
        {
            $this->parameters = $parameters;
        }

        $this->log();
    }

    /**
     * Генерирует описание ошибки в виде массива.
     *
     * @return array error structure<br>
     * [<br>
     * code<br>
     * message<br>
     * parameters[]<br>
     * ].
     */
    public function toArray () {
        return [
            'code'       => $this->code,
            'group'      => $this->group,
            'message'    => $this->message,
            'parameters' => $this->parameters
        ];
    }

    /**
     * Генерирует описание ошибки в виде строки
     *
     * @return string Описание ошибки
     */
    public function toString () {
        $message = studly_case( $this->group ) .
            ' error ' .
            $this->code .
            ': ' .
            studly_case( $this->message );

        if( $this->parameters )
        {
            $arr = [];
            foreach( $this->parameters as $kparam => $param )
            {
                $arr[] = $param;
            }

            $message.= ' (' . json_encode($arr) . ')';
        }
        return $message;
    }

    public function log () {
        Log::error($this->toString());
        $obSentry = new \Raven_Client(env('SENTRY_DSN', null));
        $obSentry->captureMessage($this->toString());
    }

}
