<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Project;

class PullTemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'git:pull:template {templates*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Стягивает обновления для проектов соответсвующих шаблонам templates на всех серверах';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $templatesArg = $this->argument('templates');
               
        $projects = Project::
            select([
                'id',
                'domain_id',
                'git_branch as branch',
                'app_key'
            ])->
            with('domain')->
            where('git_branch', '!=', '')->
            where('app_key', '!=', '');
        
        if($templatesArg[0] != 'all')
        {
            $projects = $projects->
                    whereIn('template_id', $templatesArg);
        }
        
        $projects = $projects->get();
        
        if(count($projects) === 0)
        {
            $this->error('Projects not found');
            return;
        }
        
        foreach($projects as $project)
        {
            $res = file_get_contents('http://' . $project->domain->name . '/api/console/git/pull?key=' . urlencode($project->app_key) . '&branch=' . $project->branch);
            $res = json_decode($res, true);
            
            $this->info('--------- <Git pull ' . $project->branch . '> ---------');
            if(!isset($res['status']))
            {
                $this->error('Error json answer');
                continue;
            }
            
            if($res['status'] == true)
            {
                $this->info($res['result']);
            }
            
            if($res['status'] == false)
            {
                $this->info($res['message']);
            }
            $this->info('--------- </Git pull ' . $project->branch . '> ---------');
            $this->info('');
        }
    }
}
