<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Project;
use App\Models\Ads;

class AdsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ads:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update ads blocks in news sites';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ads = Ads::get();
        
        if(count($ads) == 0)
        {
            $this->error('Ad blocks not found');
            return;
        }
        
        $projects = Project::
            select([
                'id',
                'domain_id',
                'app_key'
            ])->
            with('domain')->
            where('app_key', '!=', '')->
            get();
        
        if(count($projects) === 0)
        {
            $this->error('Projects not found');
            return;
        }
        
        $data = [];
        foreach($ads as $ad)
        {
            $data[] = [
                'name' => $ad->name,
                'body' => $ad->body,
                'show' => $ad->show,
                'description' => $ad->description
            ];
        }
        
        $data = json_encode($data);
        
        foreach($projects as $project)
        {
            $res = file_get_contents('http://' . $project->domain->name . '/api/ads/update?key=' . urlencode($project->app_key) . '&data=' . urlencode( $data ));
            $res = json_decode($res, true);
            
            $this->info('--------- <Update ads for ' . $project->domain->name . '> ---------');
            if(!isset($res['status']))
            {
                $this->error('Error json answer');
                continue;
            }
            
            if($res['status'] == true)
            {
                $this->info('Insert: ' . $res['new']);
                $this->info('Update: ' . $res['update']);
                $this->info('Delete: ' . $res['delete']);
            }
            
            if($res['status'] == false)
            {
                $this->info($res['message']);
            }
            
            $this->info('--------- </Update ads for ' . $project->domain->name . '> ---------');
            $this->info('');
        }
    }
}
