<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use App\Models\SocialPost;
use App\Models\Project;
use App\Extensions\Proxy;
use App\Models\FacebookAccount;
use Illuminate\Support\Facades\DB;
use App\Services\ProjectService;
use Illuminate\Support\Facades\Log;

class RepostFacebook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repost:facebook';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $string
     * @param null $verbosity
     */
    public function info($string, $verbosity = null)
    {
        Log::info('Repost Facebook: ' . $string);
        parent::info($string, $verbosity);
    }

    /**
     * @param string $string
     * @param null $verbosity
     */
    public function error($string, $verbosity = null)
    {
        Log::error('Repost Facebook: ' . $string);

        if( env('SENTRY_DSN', null) ) {
            $obSentry = new \Raven_Client(env('SENTRY_DSN', null));
            $obSentry->captureMessage($string);
        }
        parent::error($string, $verbosity);
    }

    public function goSelenium($url, $login, $password)
    {
        $proxy = env('PROXY', '');
        $seleniumHost = env('SELENIUM_HOST', '');
        
        if(!$proxy)
        {
            $this->error('PROXY url not found in env file');
            return false;
        }
        
        if(!$seleniumHost)
        {
            $this->error('SELENIUM_HOST not found in env file');
            return false;
        }
        
        $proxyRefresh = Proxy::refresh();
        if(!$proxyRefresh)
        {
            $this->error('Proxy refresh failed');
            return false;
        }
        else
        {
            $this->info('Proxy refresh - OK');
        }

        $capabilities = [
            WebDriverCapabilityType::BROWSER_NAME => 'firefox',
            WebDriverCapabilityType::PROXY => [
                'proxyType' => 'manual',
                'socksProxy' => $proxy,
            ]
        ];
        
        $driver = RemoteWebDriver::create($seleniumHost, DesiredCapabilities::firefox());
        
        if(!$driver)
        {
            $this->error('Cant create RemoteWebDriver firefox');
            $driver->close();
            return false;
        }
        else
        {
            $this->info('Run Firefox browser - OK');
        }
        
        try {
            $driver->manage()->timeouts()->implicitlyWait(1000);
            $driver->manage()->timeouts()->pageLoadTimeout(1000);
            $driver->manage()->timeouts()->setScriptTimeout(1000);

            //Запрос страницы с новостью
            $driver->get($url);
            $this->info('GET ' . $url . ' - OK');
            //----

            //Поиск кнопки для репостинга
            $facebooButton = $driver->findElements(WebDriverBy::className('share42-item'));
            if (!isset($facebooButton[4])) {
                $this->error('Facebook share button not found on page ' . $url);
                $driver->close();
                return false;
            }
            $facebooButton = $facebooButton[0];
            $facebooButton->click();
            $this->info('Click on facebook share button - OK');
            //----

            //Переключение на окно постинга facebook
            $driver->switchTo()->window($driver->getWindowHandles()[1]);
            $this->info('Switch to facebook window - OK');
            //----

            $sleepTime = rand(7, 15);
            $this->info('Sleep to ' . $sleepTime . ' sec...');
            sleep($sleepTime);

            //Находим поле ввода email
            $elementEmail = $driver->findElement(WebDriverBy::name('email'));
            $this->info('Find email input field - OK');
            //----

            //Находим поле ввода пароля
            $elementPassword = $driver->findElement(WebDriverBy::name('pass'));
            $this->info('Find password input field - OK');
            //----

            //Находим кнопку авторизации
            $elementButton = $driver->findElement(WebDriverBy::name('login'));
            $this->info('Find submit - OK');
            //----

            //Вводим данные для авторизации
            $elementEmail->sendKeys('wordnews.us@yahoo.com');
            $elementPassword->sendKeys('dXeQTD3h5sm?JuT2');
            //----

            //Нажимаем кнопку атворизации
            $mouse = $driver->getMouse();
            $mouse->mouseMove($elementButton->getCoordinates(), 0, 0);
            $mouse->click();
            $this->info('Click to login button - OK');
            //----

            $sleepTime = rand(2, 7);
            $this->info('Sleep to ' . $sleepTime . ' sec...');
            sleep($sleepTime);

            //Нажимаем список областей видимости
            $visiblyList = $driver->findElement(WebDriverBy::id('u_0_1u'));
            $visiblyList->click();
            //----

            $sleepTime = rand(2, 4);
            $this->info('Sleep to ' . $sleepTime . ' sec...');
            sleep($sleepTime);

            //Выбираем глобальный постинг
            $visiblyListElements = $driver->findElements(WebDriverBy::cssSelector('div.uiScrollableAreaContent ul li'));
            if (empty($visiblyListElements[1])) {
                $this->error('Cant click to global post item');
                $driver->close();
                return false;
            }
            $visiblyListElements[1]->click();
            $this->info('Select global post item - OK');
            //----

            $sleepTime = rand(1, 2);
            $this->info('Sleep to ' . $sleepTime . ' sec...');
            sleep($sleepTime);

            //Нажимаем на кнопку, постим запись
            $elementButton = $driver->findElement(WebDriverBy::name('__CONFIRM__'));
            $mouse->mouseMove($elementButton->getCoordinates(), 10, 10);
            $mouse->click();
            $this->info('Click to CONFIRM button - OK');
            //----

            $sleepTime = rand(5, 10);
            $this->info('Sleep to ' . $sleepTime . ' sec...');
            sleep($sleepTime);

            //Переходим в главное окно
            $driver->switchTo()->window($driver->getWindowHandles()[0]);
            $this->info('Switch to page window - OK');
            //----

            $driver->close();
            $this->info('Close firefox - OK');
        }
        catch (\Exception $exception)
        {
            $this->error($exception);
            shell_exec('pkill firefox');
            $this->info('Kill firefox processes - OK');
            return false;
        }
        
        return true;
        
        
        /*
         * For Reddit
         * 
        $elementEmail = $driver->findElement(WebDriverBy::xpath( '//form[@id="login-form"]/div/input[@name="user"]' ));
        $elementPassword = $driver->findElement(WebDriverBy::xpath( '//form[@id="login-form"]/div/input[@name="passwd"]' ));
        $elemenButton = $driver->findElement(WebDriverBy::xpath( '//form[@id="login-form"]/div/button' ));
        
        $elementEmail->sendKeys('vbondarenko');
        $elementPassword->sendKeys('systemizer123');
        
        $elemenButton->click();
        
        sleep(10);
        
        $driver->switchTo()->frame($driver->findElement(WebDriverBy::xpath( '//iframe[@title="recaptcha widget"]' ))) ;
        
        $capcha = $driver->findElement(WebDriverBy::id('recaptcha-anchor'));
        
        $mouse = $driver->getMouse();
        $mouse->mouseMove($capcha->getCoordinates(), 0, 0);
        
        $mouse->click();*/
        
        //$driver->close();
    }
    
    
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(!env('REPOST_FACEBOOK_ENABLE', false))
        {
            $this->info('ENV REPOST_FACEBOOK_ENABLE is false');
            return;
        }

        $facebookAccounts = FacebookAccount::get();
        
        $this->info('Selected ' . count($facebookAccounts) . ' facebook accounts');
        
        foreach($facebookAccounts as $facebookAccount)
        {
            $this->comment('<Repost to facebook account id '. $facebookAccount->id . '>');
            $project = ProjectService::getProject(SocialPost::SOCIAL_TYPE_FACEBOOK);
            
            if(!$project)
            {
                return;
            }

            $this->info('Select project with id ' . $project->id);
            
            $repostInfo = $project->getRepostInfo($facebookAccount->id, SocialPost::SOCIAL_TYPE_FACEBOOK);
            if(!$repostInfo)
            {
                $this->error('Cant get repost info for project ' . $project->id);

                $repostInfo = ['id' => 0];
            }

            $socialPost = new SocialPost();
            $socialPost->project_id = $project->id;
            $socialPost->post_id = $repostInfo['id'];
            $socialPost->social_account_id = $facebookAccount->id;
            $socialPost->social_type = SocialPost::SOCIAL_TYPE_FACEBOOK;
            if($socialPost->save())
            {
                $this->info('Success created SocialPost record');
            }
            else
            {
                $this->error('Cant create SocialPost record');
                continue;
            }

            if($repostInfo['id'] === 0)
            {
                continue;
            }

            $this->comment('<Selenium script>');
            $this->info('Post url = ' . $repostInfo['url']);
            $this->info('Facebook username = ' . $facebookAccount->username);

            $sResult = $this->goSelenium($repostInfo['url'], $facebookAccount->username, $facebookAccount->password);
            $this->comment('</Selenium script>');
            $this->comment('</Repost to facebook account id '. $facebookAccount->id . '>');
            $this->info('');
        }

    }
}
