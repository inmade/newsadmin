<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Template;

class MergeTemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'git:merge:template {templates*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Мерж всех веток(проектов), которые соответсвуют шаблонам templates с веткой шаблона';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = env('MERGE_PROJECT_PATH', '');
        
        if(!$path)
        {
            $this->error('Invalid merge project path');
            return;
        }
        
        $templatesArg = $this->argument('templates');
        
        $templates = Template::
        with(['projects' => function($query){
            $query->select([
                    'id',
                    'git_branch as branch',
                    'template_id'
                ])->
            where('git_branch', '!=', '');
        }]);
               
        if($templatesArg[0] != 'all')
        {
            $templates = $templates->
                    whereIn('id', $templatesArg);
        }
        
        $templates = $templates->get();

        foreach($templates as $template)
        {
            /*Стягиваем обновления для ветки шаблона*/
            $commands = [
                'cd ' . $path,
                'git checkout ' . $template->name,
                'git pull --no-edit origin ' . $template->name
            ];

            $this->info('--------- <Git pull ' . $template->name . '> ---------');
            $res = shell_exec(  implode( ';', $commands ) . ' 2>&1 &');
            $this->info('--------- </Git pull ' . $template->name . '> ---------');
            $this->info('');
            
            /*Мержим проекты с шаблоном*/
            foreach($template->projects as $project)
            {
                $commands = [
                    'cd ' . $path,
                    'git checkout ' . $project->branch,
                    'git pull --no-edit origin ' . $project->branch,
                    'git merge --no-edit ' . $template->name,
                    'git push origin ' . $project->branch
                ];

                $this->info('--------- <Merge ' . $project->branch . ' with ' . $template->name . '> ---------');
                $res = shell_exec(  implode( ';', $commands ) . ' 2>&1 &');
                $this->info($res);
                $this->info('--------- </Merge ' . $project->branch . ' with ' . $template->name . '> ---------');
                $this->info('');
            }
        }
    }
}
