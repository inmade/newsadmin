<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Project;
use App\Models\Template;

class MergeMaster extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'git:merge:master';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Мерж всех веток(проектов) с мастером';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = env('MERGE_PROJECT_PATH', '');
        
        if(!$path)
        {
            $this->error('Invalid merge project path');
            return;
        }
        
        $projects = Project::
            select([
                'id',
                'git_branch'
            ])->
            where('git_branch', '!=', '')->
            get();
        
        if(count($projects) === 0)
        {
            $this->error('Projects not found');
            return;
        }
        
        /*Добавляем ветки проектов*/
        $branches = [];
        foreach($projects as $project)
        {
            $branches[] = $project->git_branch;
        }
        
        /*Добавляем ветки шаблонов*/
        $templates = Template::get();
        
        foreach($templates as $template)
        {
            $branches[] = $template->name;
        }
        
        /*Стягиваем обновления для ветки master*/
        $commands = [
            'cd ' . $path,
            'git checkout master',
            'git pull --no-edit origin master'
        ];
        
        $this->info('--------- <Git pull master> ---------');
        $res = shell_exec(  implode( ';', $commands ) . ' 2>&1 &');
        $this->info('--------- </Git pull master> ---------');
        $this->info('');
        
        
        /*Мержим ветки с master*/        
        foreach($branches as $branch)
        {
            $commands = [
                'cd ' . $path,
                'git checkout ' . $branch,
                'git pull --no-edit origin ' . $branch,
                'git merge --no-edit master',
                'git push origin ' . $branch
            ];

            $this->info('--------- <Merge ' . $branch . ' with master> ---------');
            $res = shell_exec(  implode( ';', $commands ) . ' 2>&1 &');
            $this->info($res);
            $this->info('--------- </Merge ' . $branch . ' with master> ---------');
            $this->info('');
        }
    }
}
