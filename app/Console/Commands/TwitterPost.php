<?php

namespace App\Console\Commands;

use App\Extensions\Error;
use App\Extensions\Proxy;
use App\Models\SocialPost;
use App\Models\TwitterAccount;
use App\Services\ProjectService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Thujohn\Twitter\Facades\Twitter;

class TwitterPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Twitter posting';

    /**
     * Create a new command instance.
     *
     * @return void*
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(!env('REPOST_TWITTER_ENABLE', false))
        {
            $this->info('ENV REPOST_TWITTER_ENABLE is false');
            return;
        }

        // get twitter accounts
        $obTwitterAccounts = TwitterAccount::get();

        $this->info('Sected ' . count($obTwitterAccounts) . ' twitter accounts');

        foreach($obTwitterAccounts as $obTwitterAccount)
        {

            $this->comment('<Repost to twitter account id '. $obTwitterAccount->id . '>');
            $obProject = ProjectService::getProject(SocialPost::SOCIAL_TYPE_TWITTER);

            if(!$obProject)
            {
                return;
            }

            $this->info('Select project with id ' . $obProject->id);

            // get post data
            $arrRepostInfo = $obProject->getRepostInfo($obTwitterAccount->id, SocialPost::SOCIAL_TYPE_TWITTER);
            if(!$arrRepostInfo)
            {
                $this->error('Cant get repost info for project ' . $obProject->id);
                continue;
            }

            // test access key
            if( empty($obTwitterAccount->consumer_key) || empty($obTwitterAccount->consumer_secret) ||
                empty($obTwitterAccount->access_token) || empty($obTwitterAccount->access_token_secret) )
            {
                $this->error('Empty twitter access key ' . $obTwitterAccount->id);
                continue;
            }

            $sStatus = '';
            if(strlen($arrRepostInfo['url']) < 130)
            {
                $sStatus = str_limit($arrRepostInfo['title'], 140 - (strlen($arrRepostInfo['url']) + 4)) . ' ' . $arrRepostInfo['url'];
            }

            //////////////////////////// PROXY ////////////////////////////
            /*$proxy = env('PROXY', '');
            if(!$proxy)
            {
                $this->error('PROXY url not found in env file');
                return false;
            }
            $proxyRefresh = Proxy::refresh();
            if(!$proxyRefresh)
            {
                $this->error('Proxy refresh failed');
                return false;
            }
            else
            {
                $this->info('Proxy refresh - OK');
            }*/
            //////////////////////////// PROXY ////////////////////////////

            if($sStatus)
            {

                /*Post to twitter*/
                try
                {
                    $arrParams = [
                        'status' => $sStatus,
                        'format' => 'json'
                    ];

                    /*
                    if( $image )
                    {
                        $uploaded_media = Twitter::uploadMedia(['media' => File::get( public_path( trim($image->srcNormal(), '/') ) ) ]);
                        $params['media_ids'] = $uploaded_media->media_id_string;
                    }
                    */

                    // TODO testing

                    Twitter::reconfig([
                        'curl_followlocation'   => 1,
                        //'curl_proxy'            => $proxy,
                        'consumer_key'          => $obTwitterAccount->consumer_key,
                        'consumer_secret'       => $obTwitterAccount->consumer_secret,
                        'token'                 => $obTwitterAccount->access_token,
                        'secret'                => $obTwitterAccount->access_token_secret,
                    ]);


                    // twitter posting
                    Twitter::postTweet( $arrParams );

                    $socialPost = new SocialPost();
                    $socialPost->project_id = $obProject->id;
                    $socialPost->post_id = $arrRepostInfo['id'];
                    $socialPost->social_account_id = $obTwitterAccount->id;
                    $socialPost->social_type = SocialPost::SOCIAL_TYPE_TWITTER;

                    if($socialPost->save())
                    {
                        $this->info('Create SocialPost record');
                    }
                    else
                    {
                        $this->error('Cant create SocialPost record');
                        continue;
                    }


                } catch( \Exception $e )
                {

                    $this->error($e);
                    continue;

                }
            }

            $this->comment('</Repost to twitter account id '. $obTwitterAccount->id . '>');
            $this->info('');


        }
    }
}
