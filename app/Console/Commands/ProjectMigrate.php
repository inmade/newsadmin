<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Project;

class ProjectMigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Выполняет php artisan migrate для всех проектов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::
            select([
                'id',
                'domain_id',
                'app_key'
            ])->
            with('domain')->
            where('app_key', '!=', '')->
            get();
        
        if(count($projects) === 0)
        {
            $this->error('Projects not found');
            return;
        }
        
        foreach($projects as $project)
        {
            $res = file_get_contents('http://' . $project->domain->name . '/api/console/migrate?key=' . urlencode($project->app_key));
            $res = json_decode($res, true);
            
            $this->info('--------- <Migrate for ' . $project->domain->name . '> ---------');
            if(!isset($res['status']))
            {
                $this->error('Error json answer');
                continue;
            }
            
            if($res['status'] == true)
            {
                $this->info($res['result']);
            }
            
            if($res['status'] == false)
            {
                $this->info($res['message']);
            }
            $this->info('--------- </Migrate for ' . $project->domain->name . '> ---------');
            $this->info('');
        }
    }
}
