<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\RepostFacebook::class,
        Commands\MergeMaster::class,
        Commands\MergeTemplate::class,
        Commands\PullMaster::class,
        Commands\PullTemplate::class,
        Commands\ProjectMigrate::class,
        Commands\AdsUpdate::class,
        Commands\TwitterPost::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('repost:facebook')->cron('*/15 * * * *');
        $schedule->command('twitter:post')->cron('*/15 * * * *');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
