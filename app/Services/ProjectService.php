<?php

namespace App\Services;

use App\Models\Project;
use App\Models\SocialPost;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ProjectService
{

    public static function getProject($social_type)
    {

        $project = false;
        $project = Project::
        whereDoesntHave('socialPosts', function($query) use ($social_type) {
            $query->where('social_type', '=', $social_type);
        })->
        first();

        if(!$project)
        {
            $socialPost = SocialPost::
            select([
                'project_id',
                DB::raw('count(id) as cnt')
            ])->
            where('social_type', '=', $social_type)->
            groupBy('project_id')->
            orderBy('cnt')->
            first();

            if(!$socialPost)
            {
                Log::info('Social post not found');
                return false;
            }

            $project = Project::find($socialPost->project_id);

            if(!$project)
            {
                Log::info('Project with id ' . $socialPost->project_id . ' not found');
                return false;
            }
        }

        return $project;

    }


}