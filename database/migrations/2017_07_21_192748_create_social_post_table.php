<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'social_post', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments( 'id' );
            $table->unsignedInteger( 'project_id' );
            $table->unsignedInteger( 'post_id' );
            $table->integer( 'social_account_id' );
            $table->enum( 'social_type', ['facebook', 'reddit'] );
            $table->timestamps();
            
            /*Внешние ключи*/
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'social_post' );
    }
}
