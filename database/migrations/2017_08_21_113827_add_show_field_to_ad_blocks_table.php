<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowFieldToAdBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ad_blocks', function (Blueprint $table) {
            $table->boolean('show')->default(false)->after('body')->comment('Отображать рекламный блок');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ad_blocks', function (Blueprint $table) {
            $table->dropColumn('show');
        });
    }
}
