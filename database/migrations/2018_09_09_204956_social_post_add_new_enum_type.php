<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SocialPostAddNewEnumType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('social_post', function (Blueprint $table) {
            $table->dropColumn(['social_type']);
        });
        DB::statement("CREATE TYPE social_type AS ENUM ('facebook', 'reddit', 'twitter');");
        DB::statement("ALTER TABLE social_post ADD COLUMN social_type social_type;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social_post', function (Blueprint $table) {
            $table->dropColumn(['social_type']);
        });
        Schema::table('social_post', function (Blueprint $table) {
            $table->enum( 'social_type', ['facebook', 'reddit'] );
        });
        DB::statement("DROP TYPE social_type");
    }
}
