<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeBobyFieldInAdBlocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ad_blocks', function (Blueprint $table) {
            $table->text('body')->comment('Содержимое рекламного блока base64')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ad_blocks', function (Blueprint $table) {
            $table->string('body')->comment('Содержимое рекламного блока base64')->change();
        });
    }
}
