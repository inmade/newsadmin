<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectsAddColumnsSocialAccountId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->integer('twitter_account_id')->nullable(true);
            $table->integer('facebook_account_id')->nullable(true);
            $table->foreign('twitter_account_id')->references('id')->on('twitter_accounts');
            $table->foreign('facebook_account_id')->references('id')->on('facebook_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('twitter_account_id');
            $table->dropColumn('facebook_account_id');
        });
    }
}
